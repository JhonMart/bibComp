<?php 
	function dbConsulta($query){
		$conn = new mysqli("localhost", "jhon","senha","libcomp");

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 

		$res = $conn->query($query);
		return $res->fetch_all();
	}
	
	function dbQuery($query){
		$conn = new mysqli("localhost", "jhon","senha2","libcomp");

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 

		$res = $conn->query($query);
	}

	function tratar($arr){
		// // Variavel de apoio a consulta
		// $valorFinal=array();
		// // Tratamento para minizar os dados
		// foreach ($arr as $key => $value) {
		//   $atual = array();
		//   foreach ($value as $key => $value) {
		//     if(!is_numeric($key)){
		//       $atual[$key] =  $value;
		//     }
		//   }
		//   $valorFinal[] = $atual;
		// }
		return $arr;
	}

	// Usuário

	function consultarUser(){
		return tratar(dbConsulta("SELECT * 
								FROM users"));
	}

	function consultUser($user,$pass){
		$queryUser = dbConsulta("SELECT * 
								FROM users 
								WHERE user_email = '$user' 
									OR user_nick = '$user';");

		$queryLogin = dbConsulta("SELECT user_id,
										user_nick,
										user_name,
										user_email,
										user_phone,
										user_adress,
										user_photo
								FROM users 
								WHERE user_email = '$user' 
									AND user_pass = '$pass'
									OR user_nick = '$user'
								AND user_pass = '$pass'");

		if(count($queryUser)<1) return 'Usuário Desconhecido';
		elseif(count($queryLogin)<1) return 'Combinação Inválidas';
		else return tratar($queryLogin)[0];
	}

	function cadastroUser($nick,$nome,$email,$pass,$phone,$adress,$photo){
		dbQuery("INSERT INTO users
					(user_nick, user_name, user_email, user_pass, user_phone, user_adress, user_photo)
					VALUES('$nick', '$nome', '$email', '$pass', '$pone'
					, '$adress','$photo');");

		$queryUser = count(dbConsulta("SELECT * 
								 FROM users 
								 WHERE email = '$email'"));

		return $queryUser;
	}

	function atualizarUser($nick,$nome,$email,$pass,$phone,$adress,$photo,$id){
		dbQuery("UPDATE users
					SET user_nick='$nick', 
						user_name='$nome', 
						user_email='$email', 
						user_pass='$pass', 
						user_phone='$pone', 
						user_adress='$adress',
						user_photo='$photo'
					WHERE user_id=$id;");

		$queryUser = count(dbConsulta("SELECT *
										FROM users
										WHERE user_nick='$nick'
											AND user_name='$nome'
											AND user_email='$email'
											AND user_pass='$pass'
											AND user_phone='$pone'
											AND user_adress='$adress'
											AND user_photo='$photo'
											AND user_id=$id;"));

		return $queryUser;
	}

	function verificarUser(){
		return tratar(dbConsulta("SELECT id
								FROM users
								WHERE user_nick='$nick'
								OR user_email='$email'"))[0];
	}

	function cadastrarInteresseUser($user,$area){
		dbQuery("INSERT INTO relac_user_area
					(relac_user_area_id,relac_user_user_id) 
					VALUES($user,$area)");
		
		return count(dbConsulta("SELECT *
						FROM relac_user_area
						WHERE relac_user_area_id = $user
							AND relac_user_user_id = $area"));
	}

	function excluirUser($id){
		dbQuery("DELETE FROM users
					WHERE user_id=$id");

		return !count(dbConsulta("SELECT *
								FROM users
								WHERE user_id=$id"));
	}

	// LIVRO

	function consultaBook($name=null,$edition=null,$author=null){
		if($name && $edition && $author)
			$queryBook = tratar(dbConsulta("SELECT * 
								 			FROM book"));
		else
			$queryBook = tratar(dbConsulta("SELECT * 
								 			FROM book
								 			WHERE book_name='$name'
									 			AND	book_edition='$edition'
												AND	book_author='$author'"));

		return $queryBook;
	}

	function cadastroBook($name,$edition,$author,$owner){
		dbQuery("INSERT INTO book
					(book_name, book_edition, book_author, book_owner)
					VALUES('$name', '$edition', '$author', 0);");

		$queryBook = count(dbConsulta("SELECT *
										FROM book
										WHERE book_name='$name'
											AND	book_edition='$edition'
											AND	book_author='$author'
											AND	book_owner=$owner"));

		return $queryBook;
	}


	function atualizarBook($name,$edition,$author,$owner,$id){
		dbQuery("UPDATE book
					SET book_name='$name',
						book_edition='$edition',
						book_author='$author',
						book_owner=$owner
					WHERE book_id=$id;");

		$queryBook = count(dbConsulta("SELECT *
										FROM book
										WHERE book_name='$name'
											AND	book_edition='$edition'
											AND	book_author='$author'
											AND	book_owner=$owner
											AND book_id=$id;"));

		return $queryBook;
	}

	function verificarBook($name,$author){
		return tratar(dbConsulta("SELECT *
								FROM book
								WHERE user_nick='$name'
								OR user_email='$author'"));
	}

	function cadastrarInteresseBook($book,$area){
		dbQuery("INSERT INTO relac_book_area
					(relac_book_book_id,relac_book_area_id) 
					VALUES($book,$area)");
		
		return count(dbConsulta("SELECT *
						FROM relac_book_area
						WHERE relac_book_book_id = $book
							AND relac_book_area_id = $area"));
	}

	function excluirBook($id){
		dbQuery("DELETE FROM book
					WHERE book_id=$id");

		return !count(dbConsulta("SELECT *
								FROM book
								WHERE book_id=$id"));
	}

	// Emprestimo

	function consultarLoan(){
		$queryLoan = tratar(dbConsulta("SELECT loan_id,
												loan_book,
												loan_time,
												loan_receive,
												loan_owner,
												loan_create,
										FROM lending
										WHERE book_id=$id"));
	}

	function cadastrarLoan($book,$time){
		dbQuery("INSERT INTO lending
					(loan_book, loan_time)
					VALUES($book, $time);");

		$queryLoan = count(dbConsulta("SELECT *
										FROM lending
										WHERE loan_book='$book'
											AND	book_edition='$time'"));

		return $queryLoan;
	}

	function entregueOwnerLoan($book,$time,$status){
		dbQuery("UPDATE lending
					SET loan_owner=$status
					WHERE loan_book='$book'
						AND	book_edition='$time'");

		$queryLoan = count(dbConsulta("SELECT *
										FROM lending
										WHERE loan_owner='$book'
											AND	book_edition='$time'
											AND loan_book=$status"));

		return $queryLoan;
	}

	function entregueReceiverLoan($book,$time,$status){
		dbQuery("UPDATE lending
					SET loan_receive=$status
					WHERE loan_book='$book'
						AND	book_edition='$time'");

		$queryLoan = count(dbConsulta("SELECT *
										FROM lending
										WHERE loan_receive='$book'
											AND	book_edition='$time'
											AND loan_book=$status"));

		return $queryLoan;
	}

	function excluirLoan($id){
		dbQuery("DELETE FROM lending
					WHERE loan_id=$id");

		return !count(dbConsulta("SELECT *
								FROM lending
								WHERE loan_id=$id"));
	}

	// Interresse

	function consultaArea(){
		return tratar(dbConsulta("SELECT *
								FROM area_inter"));
	}

	function cadastrarArea($name){
		dbQuery("INSERT INTO area_inter
					(area_name)
					VALUES('$name');");

		$queryLoan = count(dbConsulta("SELECT *
										FROM area_inter
										WHERE area_name='$name'"));

		return $queryLoan;
	}

	function excluirArea($id){
		dbQuery("DELETE FROM area_inter
					WHERE area_id=$id");

		return !count(dbConsulta("SELECT *
								FROM area_inter
								WHERE area_id=$id"));
	}
?>