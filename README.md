
# Biblioteca Computação

Backend Completo API

⚠️ Como usar:

## Listar Todos as Sessões
```java_script
$.post('LINK_API',{key:'CHAVE_API',list:{sessions:''}}).done(resp=>{
	console.log(resp); // Saida aqui
});
```

## Entrando na conta
```java_script
$.post('LINK_API',{key:'CHAVE_API',login:{user_name:'USER_EMAIL or USER_NICK',
                                          user_pass:'SENHAR_USER'}})
.done(resp=>{
	console.log(resp); // Saida aqui
})
```

## Usando o TOKEN
```java_script
$.post('LINK_API',{key:'CHAVE_API',session:'TOKEN'})
.done(resp=>{
	console.log(resp); // Saida aqui
})
```

## Sair e Excluir a Sessão
```java_script
$.post('LINK_API',{key:'CHAVE_API',session:'TOKEN',logout:''})
.done(resp=>{
	console.log(resp); // Saida aqui
})
```

## `Excluir Usuário`
```java_script
$.post('LINK_API',{key:'CHAVE_API',drop:{user:ID_USER}})
.done(resp=>{
	console.log(resp); // Saida aqui
})
```

### O atributo DROP é compativel com:
> Usuário, Livro, Área de Interesse e Emprestimo usando os respectivos subatributo [user, book, area, loan] passando como valor o ID do mesmo. 

## Listar todos os Usuários
```java_script
$.post('LINK_API',{key:'CHAVE_API',list:{user:''}})
.done(resp=>{
	console.log(resp); // Saida aqui
})
```

### O atributo LIST é compativel com:
> Usuários, Livros, Áreas de Interesse e Emprestimos usando os respectivos subatributo [user, book, area, loan] passando como valor o string vazia. 

## Adicionar uma Área de Interesse para um Usuário
```java_script
$.post('LINK_API',{key:'CHAVE_API',register:{interesse_user:ID_USER, interesse_area: ID_AREA}})
.done(resp=>{
	console.log(resp); // Saida aqui
})
```

### O atributo REGISTER é compativel com:
> Usuários, Livros, Áreas de Interesse e Emprestimos usando os respectivos subatributo condizentes com seus campos no formulario e passando como valor suas informações. 

#### Exemplo de REGISTER com Usuário
```java_script
$.post('LINK_API',{key:'CHAVE_API',register:{user_name: 'NOME_USER', 
                                            user_nick: 'APELIDO_USER',
                                            user_email: 'EMAIL_USER',
                                            user_pass: 'SENHA_USER',
                                            user_phone: 'FONE_USER',
                                            user_adress: 'ENDERECO_USER',
                                            user_photo: 'FOTO_USER'}})
.done(resp=>{
	console.log(resp); // Saida aqui
})
```

#### Exemplo de UPDATE com Usuário
```java_script
$.post('LINK_API',{key:'CHAVE_API',update:{user_name: 'NOME_USER', 
                                            user_nick: 'APELIDO_USER',
                                            user_email: 'EMAIL_USER',
                                            user_pass: 'SENHA_USER',
                                            user_phone: 'FONE_USER',
                                            user_adress: 'ENDERECO_USER',
                                            user_photo: 'FOTO_USER'
                                            user_id: ID_USER}})
.done(resp=>{
	console.log(resp); // Saida aqui
})
```

### `TODOS OS PARAMETROS DEVEM SER RECEBIDOS`
> Caso falte algum campo a consulta, cadastro ou atualização não seram realizados, devem ser mandados todos mesmo que seja como valor vazio.

> Duvidas Consultar o arquivo [integrador.php](https://gitlab.com/JhonMart/bibComp/raw/master/integrador.php).

Link para a atual [API](https://jonatas777gato.000webhostapp.com).