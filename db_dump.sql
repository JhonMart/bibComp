-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: bib_comp
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area_inter`
--

DROP TABLE IF EXISTS `area_inter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area_inter` (
  `area_id` int(10) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`area_id`),
  UNIQUE KEY `area_name` (`area_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area_inter`
--

LOCK TABLES `area_inter` WRITE;
/*!40000 ALTER TABLE `area_inter` DISABLE KEYS */;
/*!40000 ALTER TABLE `area_inter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `book_id` int(10) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(120) DEFAULT NULL,
  `book_edition` varchar(50) DEFAULT NULL,
  `book_author` varchar(100) DEFAULT NULL,
  `book_owner` int(10) DEFAULT NULL,
  PRIMARY KEY (`book_id`),
  KEY `book_owner` (`book_owner`),
  CONSTRAINT `book_ibfk_1` FOREIGN KEY (`book_owner`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lending`
--

DROP TABLE IF EXISTS `lending`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lending` (
  `loan_id` int(10) NOT NULL AUTO_INCREMENT,
  `loan_book` int(10) DEFAULT NULL,
  `loan_time` int(10) DEFAULT NULL,
  `loan_create` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`loan_id`),
  KEY `loan_book` (`loan_book`),
  CONSTRAINT `lending_ibfk_1` FOREIGN KEY (`loan_book`) REFERENCES `book` (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lending`
--

LOCK TABLES `lending` WRITE;
/*!40000 ALTER TABLE `lending` DISABLE KEYS */;
/*!40000 ALTER TABLE `lending` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relac_book_area`
--

DROP TABLE IF EXISTS `relac_book_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relac_book_area` (
  `relac_book_area_id` int(10) DEFAULT NULL,
  `relac_book_book_id` int(10) DEFAULT NULL,
  KEY `relac_book_area_id` (`relac_book_area_id`),
  KEY `relac_book_book_id` (`relac_book_book_id`),
  CONSTRAINT `relac_book_area_ibfk_1` FOREIGN KEY (`relac_book_area_id`) REFERENCES `area_inter` (`area_id`),
  CONSTRAINT `relac_book_area_ibfk_2` FOREIGN KEY (`relac_book_book_id`) REFERENCES `book` (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relac_book_area`
--

LOCK TABLES `relac_book_area` WRITE;
/*!40000 ALTER TABLE `relac_book_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `relac_book_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relac_user_area`
--

DROP TABLE IF EXISTS `relac_user_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relac_user_area` (
  `relac_user_area_id` int(10) DEFAULT NULL,
  `relac_user_user_id` int(10) DEFAULT NULL,
  KEY `relac_user_area_id` (`relac_user_area_id`),
  KEY `relac_user_user_id` (`relac_user_user_id`),
  CONSTRAINT `relac_user_area_ibfk_1` FOREIGN KEY (`relac_user_area_id`) REFERENCES `area_inter` (`area_id`),
  CONSTRAINT `relac_user_area_ibfk_2` FOREIGN KEY (`relac_user_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relac_user_area`
--

LOCK TABLES `relac_user_area` WRITE;
/*!40000 ALTER TABLE `relac_user_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `relac_user_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_nick` varchar(100) DEFAULT NULL,
  `user_name` varchar(120) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_pass` varchar(200) DEFAULT NULL,
  `user_phone` varchar(50) DEFAULT NULL,
  `user_adress` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'bib_comp'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-16 11:21:17
