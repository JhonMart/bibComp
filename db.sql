CREATE DATABASE IF NOT EXISTS bib_comp;
CREATE TABLE bib_comp.users(
	user_id int(10) AUTO_INCREMENT PRIMARY KEY,
	user_nick varchar(100),
	user_name varchar(120),
	user_email varchar(100),
	user_pass varchar(200),
	user_phone varchar(50),
	user_adress varchar(80),
	user_photo varchar(500)
);
CREATE TABLE bib_comp.area_inter(
	area_id int(10) AUTO_INCREMENT PRIMARY KEY,
	area_name varchar(50) UNIQUE
);
CREATE TABLE bib_comp.book(
	book_id int(10) AUTO_INCREMENT PRIMARY KEY,
	book_name varchar(120),
	book_edition varchar(50),
	book_author varchar(100),
	book_owner int(10),
	book_photo varchar(500),
	FOREIGN KEY (book_owner) REFERENCES bib_comp.users(user_id)
);
CREATE TABLE bib_comp.lending(
	loan_id int(10) AUTO_INCREMENT PRIMARY KEY,
	loan_book int(10),
	loan_time int(10),
	loan_receive boolean,
	loan_owner boolean,
	loan_create DATETIME default now(),
	FOREIGN KEY (loan_book) REFERENCES bib_comp.book(book_id)
);
CREATE TABLE bib_comp.relac_user_area(
	relac_user_area_id int(10),
	relac_user_user_id int(10),
	FOREIGN KEY (relac_user_area_id) REFERENCES bib_comp.area_inter(area_id),
	FOREIGN KEY (relac_user_user_id) REFERENCES bib_comp.users(user_id),
	UNIQUE (relac_user_area_id,relac_user_user_id)
);
CREATE TABLE bib_comp.relac_book_area(
	relac_book_area_id int(10),
	relac_book_book_id int(10),
	FOREIGN KEY (relac_book_area_id) REFERENCES bib_comp.area_inter(area_id),
	FOREIGN KEY (relac_book_book_id) REFERENCES bib_comp.book(book_id),
	UNIQUE (relac_book_area_id,relac_book_book_id)
);