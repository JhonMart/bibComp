<?php
	include 'confDB.php';
	header('Content-Type: application/json; charset=utf-8');
	$saida = [];

	if(isset($_POST['key'])){
 		header("Access-Control-Allow-Origin: *"); // Permissão para acesso
		if(base64_decode($_POST['key'])=='EssaEhAChaveSecreta'){ // Validando Token
 			if(isset($_POST['login'])){ // Validar entrada de função
				 if(!isset($_POST['login']['user_name'])) $saida['err'][] = 'Usuário não recebido';
				 elseif(!isset($_POST['login']['user_pass'])) $saida['err'][] = 'Senha não recebida';
				 else{
					$user_name = $_POST['login']['user_name']; // Recebendo Usuário
					$user_pass = $_POST['login']['user_pass']; // Recebendo Senha
						
					$saida['login'] = valLogin($user_name, $user_pass);
				 }

 			}elseif(isset($_POST['list']['sessions'])){
				$saida['lista_user'] = array_slice(scandir(session_save_path()),2);
			}elseif(isset($_POST['logout'])){
				if(!isset($_POST['session'])) $saida['err'] = 'Sessão não recebida';
				else{
					$key = $_POST['session'];
					if(valToken($key)){
						session_id($id);
						session_start();
	
						$saida['status'] = session_destroy()? 'Offline' : 'Online';
					}else $saida['status'] = 'Falha na validaçãos da sessão';
				}
			}elseif(isset($_POST['session'])){
				$key = $_POST['session'];
				$saida['login']['data'] = valToken($key)? valLogin('','',$key) : 'Sessão Inválido';
			}elseif(isset($_POST['register']['user_name'])){
				if(!isset($_POST['register']['user_name'])) $saida['err'][] = 'Usuário não recebido';
				elseif(!isset($_POST['register']['user_nick'])) $saida['err'][] = 'Apelido não recebido';	 
				elseif(!isset($_POST['register']['user_email'])) $saida['err'][] = 'E-mail não recebido';
				elseif(!isset($_POST['register']['user_pass'])) $saida['err'][] = 'Senha não recebida';
				elseif(!isset($_POST['register']['user_phone'])) $saida['err'][] = 'Telefone não recebido';
				elseif(!isset($_POST['register']['user_adress'])) $saida['err'][] = 'Endereço não recebido';
				elseif(!isset($_POST['register']['user_photo'])) $saida['err'][] = 'Foto não recebida';
				else{
					$nome = $_POST['register']['user_name'];  // Recebendo Nome do Usuário
					$nick = $_POST['register']['user_nick']; // Recebendo Apelido do Usuário
					$email = $_POST['register']['user_email']; // Recebendo Email do Usuário
					$pass = md5($_POST['register']['user_pass']); // Recebendo e Cripografando Senha do Usuário
					$phone = $_POST['register']['user_phone']; // Recebendo Telefone do Usuário
					$adress = $_POST['register']['user_adress']; // Recebendo Endereço do Usuário
					$photo = $_POST['register']['user_photo']; // Recebendo Foto do Usuário

					$saida['register'] = cadastroUser($nick,$nome,$email,$pass,$phone,$adress,$photo);
				}

			}elseif(isset($_POST['register']['area_name'])){
				$name = $_POST['register']['area_name'];

				$saida['register'] = cadastrarArea($name);
			}elseif(isset($_POST['register']['book_name'])){
				if(!isset($_POST['register']['book_edition'])) $saida['err'][] = 'Edição não recebida';
				if(!isset($_POST['register']['book_author'])) $saida['err'][] = 'Autor não recebido';
				if(!isset($_POST['register']['book_owner'])) $saida['err'][] = 'Dono não recebido';
				else{
					$name = $_POST['register']['book_name'];
					$edition = $_POST['register']['book_edition'];
					$author = $_POST['register']['book_author'];
					$owner = $_POST['register']['book_owner'];
	
					$saida['register'] = cadastroBook($name,$edition,$author,$owner);
				}
				
			}elseif(isset($_POST['register']['loan_time'])){
				if(!isset($_POST['register']['loan_book'])) $saida['err'][] = 'Livro não recebido';
				else{
					$book = $_POST['register']['loan_book'];
					$time = $_POST['register']['loan_time'];
	
					$saida['register'] = cadastrarLoan($book,$time);
				}

			}elseif(isset($_POST['register']['interesse_user'])){
				if(!isset($_POST['register']['interesse_area'])) $saida['err'][] = 'Área não recebida';
				else{
					$user = $_POST['register']['interesse_user'];
					$area = $_POST['register']['interesse_area'];
	
					$saida['register'] = cadastrarInteresseUser($user,$area);
				}
				
			}elseif(isset($_POST['register']['interesse_book'])){
				if(!isset($_POST['register']['interesse_area'])) $saida['err'][] = 'Área não recebida';
				else{
					$book = $_POST['register']['interesse_book'];
					$area = $_POST['register']['interesse_area'];

					$saida['register'] = cadastrarInteresseBook($book,$area);
				}

			}elseif(isset($_POST['update']['user_name'])){
				if(!isset($_POST['update']['user_name'])) $saida['err'][] = 'Usuário não recebido';
				elseif(!isset($_POST['update']['user_nick'])) $saida['err'][] = 'Apelido não recebido';	 
				elseif(!isset($_POST['update']['user_email'])) $saida['err'][] = 'E-mail não recebido';
				elseif(!isset($_POST['update']['user_pass'])) $saida['err'][] = 'Senha não recebida';
				elseif(!isset($_POST['update']['user_phone'])) $saida['err'][] = 'Telefone não recebido';
				elseif(!isset($_POST['update']['user_adress'])) $saida['err'][] = 'Endereço não recebido';
				elseif(!isset($_POST['update']['user_photo'])) $saida['err'][] = 'Foto não recebida';
				elseif(!isset($_POST['update']['user_id'])) $saida['err'][] = 'ID não recebido';
				else{
					$nome = $_POST['update']['user_name'];  // Recebendo Nome do Usuário
					$nick = $_POST['update']['user_nick']; // Recebendo Apelido do Usuário
					$email = $_POST['update']['user_email']; // Recebendo Email do Usuário
					$pass = md5($_POST['update']['user_pass']); // Recebendo e Cripografando Senha do Usuário
					$phone = $_POST['update']['user_phone']; // Recebendo Telefone do Usuário
					$adress = $_POST['update']['user_adress']; // Recebendo Endereço do Usuário
					$photo = $_POST['update']['user_photo']; // Recebendo Foto do Usuário
					$id = $_POST['update']['user_id']; // Recebendo ID do Usuário

					$saida['update'] = atualizarUser($nick,$nome,$email,$pass,$phone,$adress,$photo,$id);
				}
				
			}elseif(isset($_POST['list']['user'])){
				$saida['user'] = consultarUser();
			}elseif(isset($_POST['list']['area'])){
				$saida['area'] = consultaArea();
			}elseif(isset($_POST['list']['book'])){
				$saida['book'] = consultaBook();
			}elseif(isset($_POST['list']['loan'])){
				$saida['loan'] = consultarLoan();
			}elseif(isset($_POST['drop']['user'])){
				$id = $_POST['drop']['user'];

				$saida['user'] = excluirUser($id);
			}elseif(isset($_POST['drop']['area'])){
				$id = $_POST['drop']['area'];
				
				$saida['area'] = excluirArea($id);
			}elseif(isset($_POST['drop']['book'])){
				$id = $_POST['drop']['book'];
				
				$saida['book'] = excluirBook($id);
			}elseif(isset($_POST['drop']['loan'])){
				$id = $_POST['drop']['loan'];
				
				$saida['loan'] = excluirLoan($id);
			}elseif(isset($_POST['drop']['loan'])){
				$id = $_POST['drop']['loan'];
				
				$saida['loan'] = excluirLoan($id);
			}elseif(isset($_POST['owner']['deliver_status'])){
				if(!isset($_POST['owner']['deliver_book'])) $saida['err'][] = 'Livro não recebido';
				if(!isset($_POST['owner']['deliver_time'])) $saida['err'][] = 'Tempo não recebido';
				else{
					$book = $_POST['owner']['deliver_book'];
					$time = $_POST['owner']['deliver_time'];
					$status = $_POST['owner']['deliver_status'];
					
					$saida['deliver'] = entregueReceiverLoan($book,$time,$status); // O Usuário dono confirma ou não que Recebeu o Livro
				}
				
			}elseif(isset($_POST['addressee']['deliver_status'])){
				if(!isset($_POST['addressee']['deliver_book'])) $saida['err'][] = 'Livro não recebido';
				if(!isset($_POST['addressee']['deliver_time'])) $saida['err'][] = 'Tempo não recebido';
				else{
					$book = $_POST['addressee']['deliver_book'];
					$time = $_POST['addressee']['deliver_time'];
					$status = $_POST['addressee']['deliver_status'];
					
					$saida['deliver'] = entregueOwnerLoan($book,$time,$status); // Quem fez o emprestimo confirma ou não que mandou o Livro
				}

			}else{
				$saida['err'][] = 'Entrada desconhecida';
			}
		}else{
			$saida['err'][] = 'Token inválido';
		}

		echo json_encode($saida);
	}

	function valLogin($user,$pass,$token=null){
		$data = consultUser($user,md5($pass));
		if($token){
			session_id($token);
			session_start();
			return $_SESSION;
		}elseif(count($data)>1){
			$id = uniqid();
			session_id($id);
			session_start();
			$_SESSION['login'] = $data;
			return $id;
		}else{
			return $data;
		}
	}

	function valToken($key){
		return in_array("sess_$key",scandir(session_save_path()));
	}